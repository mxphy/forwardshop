﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace fShop
{
    public partial class Inventario : Form
    {
        public Inventario()
        {
            InitializeComponent();
        }

        NuevoProducto prod;

        private void Button6_Click(object sender, EventArgs e)
        {
            prod = new NuevoProducto();
            prod.Show();
        }

        private void Inventario_Load(object sender, EventArgs e)
        {
            InventarioList.DataSource = getDataInventory();
        }

        private DataTable getDataInventory()
        {
            DataTable inventory = new DataTable();

            using (MySqlConnection connection = new MySqlConnection(Database.Main.getConnectionString()))
            {
                connection.Open();
                MySqlCommand command = connection.CreateCommand();
                command.CommandText = "SELECT * FROM inventory";

                using (MySqlDataReader reader = command.ExecuteReader())
                {
                    inventory.Load(reader);
                }
            }

            return inventory;
        }
    }
}
