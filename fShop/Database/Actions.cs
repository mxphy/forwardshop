﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using fShop.Classes;
using System.Windows.Forms;


namespace fShop.Database
{
    class Actions
    {
        // Creamos una función para revisar si el login es correcto || Tipo bool porque retorna true or false
        public static bool checkLoginDetails(string username, string password)
        {
            // Creamos la variable y asignamos false
            bool check = false;
            // Creamos una variable para almacenar la contraseña encriptada con SHA512
            string passwd = Utilities.Actions.GenerateSHA512String(password);
            // Hacemos conexión con el servidor MySQL
            using (MySqlConnection connection = new MySqlConnection(Main.getConnectionString()))
            {
                connection.Open();
                string query = "SELECT * FROM accounts WHERE user = @username AND pass = @password";
                MySqlCommand command = new MySqlCommand(query, connection);
                // Query
                //command.CommandText = "SELECT * FROM accounts WHERE user = @username AND pass = @password";
                command.Parameters.AddWithValue("@username", username);
                command.Parameters.AddWithValue("@password", passwd);

                MySqlDataReader reader = command.ExecuteReader();
                // Le damos a la variable check el valor HasRows (si tiene rows devuelve true, si no devuelve false)
                check = reader.HasRows;
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        int id = reader.GetInt32("id");
                        string email = reader.GetString("email");
                        string name = reader.GetString("name");

                        Classes.User.idUser = id;
                        Classes.User.userName = username;
                        Classes.User.emailAddress = email;
                        Classes.User.nameUser = name;
                    }
                }
                // Devolvemos la variable check
                return check;
            }
        }

        public static void createProduct(string name, int units, int price, string barcode)
        {
            using (MySqlConnection connection = new MySqlConnection(Main.getConnectionString()))
            {
                connection.Open();
                MySqlCommand command = connection.CreateCommand();
                command.CommandText = "INSERT INTO products (name, units, price, barcode) VALUES (@name, @units, @price, @barcode)";
                command.Parameters.AddWithValue("@name", name);
                command.Parameters.AddWithValue("@units", units);
                command.Parameters.AddWithValue("@price", price);
                command.Parameters.AddWithValue("@barcode", barcode);

                command.ExecuteNonQuery();
            }
        }

    }
}
