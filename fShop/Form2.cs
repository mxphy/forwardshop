﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using fShop.Classes;

namespace fShop
{
    public partial class Logged : Form
    {
        public Logged()
        {
            InitializeComponent();

            labelbienvenida.Text = $"¡Bienvenido, {Classes.User.nameUser}!";
        }

        ForwardShop shop;
        Inventario inv;

        private void Button7_Click(object sender, EventArgs e)
        {
            shop = new ForwardShop();
            shop.Show();
            this.Hide();

            Classes.User.idUser = 0;
            Classes.User.userName = "";
            Classes.User.emailAddress = "";
            Classes.User.nameUser = "";
        }

        private void Button3_Click(object sender, EventArgs e)
        {
            inv = new Inventario();
            inv.Show();
            this.Hide();
        }
    }
}
