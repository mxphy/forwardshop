﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Text;

namespace fShop.Utilities
{
    class Http
    {
        // Funcion para crear un weclient 
        public static byte[] Post(string url, NameValueCollection pairs)
        {
            using (WebClient webClient = new WebClient())
                return webClient.UploadValues(url, pairs);
        }

        // Función para enviar el webhook
        public static void sendWebHook(int type, string message)
        {
            string url = "";
            string username = "";
            switch (type)
            {
                case 1:
                    url = "https://discordapp.com/api/webhooks/603336611677208576/xQ32J3juSICYlyM8nOJvKovQahcplOSWu7EOma96TVJVzjfxiwXyXXe_sETpqwETSFtI";
                    username = "ForwardShop";
                    break;
            }

            Http.Post(url, new NameValueCollection()
            {
                {
                    "username",
                    username
                },
                {
                    "content",
                    message
                }
            });
        }
    }
}