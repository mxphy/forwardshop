﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using System.Windows.Forms;
using fShop.Classes;
using WebSocket4Net;

namespace fShop
{
    public partial class ForwardShop : Form
    {
        private static WebSocket websocket;
        private static int piracy;
        private static int conexion = 0;
        public ForwardShop()
        {
            InitializeComponent();

            // Iniciamos cliente websocket
            websocket = new WebSocket("ws://localhost:8088/");
            websocket.Opened += new EventHandler(websocket_Opened);
            websocket.Error += new EventHandler<SuperSocket.ClientEngine.ErrorEventArgs>(websocket_Error);
            websocket.Closed += new EventHandler(websocket_Closed);
            websocket.MessageReceived += new EventHandler<WebSocket4Net.MessageReceivedEventArgs>(websocket_MessageReceived);
            websocket.Open();
        }

        // Definimos la ventada del logeo
        Logged logeo;

        // Definimos la ventana del pirateo
        private static Pirate pirata;

        DialogResult result;

        // Función al clickar el botón
        private void Button1_Click(object sender, EventArgs e)
        {
            // Checkeamos si tiene copia pirata
            if(conexion == 0)
            {
                if (piracy == 0)
                {
                    // Comprobamos si los inputs tienen texto
                    if (username.Text != "" && password.Text != "")
                    {
                        // Comprobamos si los datos son correctos
                        if (Database.Actions.checkLoginDetails(username.Text, password.Text))
                        {
                            // Mostramos la otra ventana y cerramos esta
                            logeo = new Logged();
                            logeo.Show();
                            this.Hide();
                        }
                        else
                        {
                            MessageBoxButtons button = MessageBoxButtons.OK;
                            result = MessageBox.Show("Los datos son incorrectos", "Error", button);
                        }
                    }
                }
                else if (piracy == 1)
                {
                    this.Hide();
                    pirata = new Pirate();
                    pirata.Show();
                }
            }
            else if(conexion == 1)
            {
                MessageBoxButtons button = MessageBoxButtons.OK;
                result = MessageBox.Show("No se ha podido conectar con el servidor de ForwardDevs", "Error", button);
            }
        }

        private void websocket_MessageReceived(object sender, MessageReceivedEventArgs e)
        {
            string mensaje = e.Message;
            if (mensaje == "OK")
                piracy = 0;
            else
            {
                piracy = 1;
                MessageBoxButtons button = MessageBoxButtons.OK;
                result = MessageBox.Show("No se admiten copias piratas", "Aviso", button);
            }
        }

        private static void websocket_Closed(object sender, EventArgs e)
        {
            
        }

        private static void websocket_Opened(object sender, EventArgs e)
        {
            string license = File.ReadAllText(@"license.txt");

            websocket.Send(license);
        }

        private static void websocket_Error(object sender, EventArgs e)
        {
            conexion = 1;
        }

    }
}
