﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fShop.Database
{
    class Main
    {
        // Creamos una función para obtener los datos de la DB para realizar cualquier consulta
        public static string getConnectionString()
        {
            string datos = File.ReadAllText(@"database.txt", Encoding.UTF8);

            return datos;
        }
    }
}
