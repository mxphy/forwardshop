﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace fShop
{
    public partial class NuevoProducto : Form
    {
        public NuevoProducto()
        {
            InitializeComponent();
        }

        DialogResult resulta;

        private void Button7_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Button6_Click(object sender, EventArgs e)
        {
            if(box1.Text != "" && box2.Value != 0 && box3.Value != 0 && box4.Text != "")
            {
                Database.Actions.createProduct(box1.Text, (int)box2.Value, (int)box3.Value, box4.Text);
                this.Close();
                MessageBoxButtons button = MessageBoxButtons.OK;
                resulta = MessageBox.Show("Has creado el nuevo producto con éxito", "Información", button);
            }
        }
    }
}
